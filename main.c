#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <ulimit.h>
#include <unistd.h>

#define PATH_LENGTH 255

int main(int argc, char* argv[], char *envp[]) {
  int i, j;
  struct rlimit rl_core;
  char path[PATH_LENGTH];
  opterr = 0;
  int arg;
  while ((arg = getopt (argc, argv, "ispuU:cC:dvV:")) != -1)
    switch (arg)
    {
    case 'i':
      printf("User ID: %d\n", getuid());
      printf("Effective User ID: %d\n", geteuid());
      break;

    case 's':
      setpgid(getpid(), 0);
      break;

    case 'p':
    //  if (getpgid(getpid()) == -1) {
    //    fprintf(stderr, "ERROR: getppid().\n");
    //    exit(errno);
    //    }
      printf("Process ID: %d\n", getpid());
      printf("Parent Process ID: %d\n", getppid());
      printf("Process Group ID: %d\n", getpgid(getpid()));
      break;


    case 'u':
      if (ulimit(UL_GETFSIZE) == -1) {
        fprintf(stderr, "ERROR: ulimit(UL_GETFSIZE).\n");
        exit(errno);
      }
      printf("ulimit: %ld\n", ulimit(UL_GETFSIZE));
      break;


    case 'U':
      if (ulimit(UL_SETFSIZE, atol(optarg)) == -1) {
        fprintf(stderr, "ERROR: ulimit(UL_SETFSIZE, %ld).\n", atol(argv[i]));
        exit(errno);
      }
      break;


    case 'c':
      if (getrlimit(RLIMIT_CORE, &rl_core) == -1) {
        fprintf(stderr, "ERROR: getrlimit(RLIMIT_CORE, &rl_core).\n");
        exit(errno);
      }
      printf("Core-file size: %ld\n", rl_core.rlim_cur);
      break;


    case 'C':
      if (getrlimit(RLIMIT_CORE, &rl_core) == -1) {
        fprintf(stderr, "ERROR: getrlimit(RLIMIT_CORE, &rl_core).\n");
        exit(errno);
      }
      rl_core.rlim_cur = atol(optarg);
      if (setrlimit(RLIMIT_CORE, &rl_core) == -1) {
        fprintf(stderr, "ERROR: setrlimit(RLIMIT_CORE, &rl_core).\n");
        exit(errno);
      }
      break;


    case 'd':
      if (getcwd(path, PATH_LENGTH) == NULL) {
        fprintf(stderr, "ERROR: getcwd(path, %d).\n", PATH_LENGTH);
        exit(errno);
      }
      printf("Current dir: %s\n", path);
      break;


    case 'v':
        printf("Environment Variables:\n");
        for (j = 0; envp[j] != NULL; j++) {
          printf("%s\n", envp[j]);
        }
        break;


     case 'V':{
       if (putenv(optarg) != 0) {
         fprintf(stderr, "putenv(): ERROR: putenv(%s).\n", optarg);
         exit(errno);
       }
       break;
     }

    case '?': {
       if (optopt == 'U')
         fprintf(stderr, "Option -%U requires an argument.\n", optopt);
       else if (optopt == 'C')
         fprintf(stderr, "Option -%C requires an argument.\n", optopt);
       else if (optopt == 'V')
         fprintf(stderr, "Option -%V requires an argument.\n", optopt);
       else if (isprint(optopt))
         fprintf(stderr, "Unknown option `-%c'.\n", optopt);
       else
         fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
       return 1;
     }
    default:
      abort ();
    }
  return 0;
}
